#include <stdio.h>
#include <string.h>
#include <stdlib.h>     
#include <stdint.h>
#include "utility.c"

uint8_t PHT;
uint8_t predictors[256];
uint8_t bytes[256];

void init(){
    PHT = 0b00000000;
    memset(predictors, 0, sizeof(bytes));
    memset(bytes, 0, sizeof(bytes));
}


bool predict(unsigned int pc){

    uint8_t pc_low_8 = (pc >> 24) & 0xff;
    if(predictors[pc_low_8] > 0b01){
        return true;
    }
    return false;
}

void train(unsigned int pc, bool outcome){
    /*if(PHT != 0b11111111)*/
        /*printf("%s\n", byte_to_binary(PHT));*/
    PHT = push_shifter(PHT, outcome);
    uint8_t pc_low_8 = (pc >> 24) & 0xff;
    uint8_t predictor_address = PHT ^ pc_low_8;
    uint8_t predictor_value = predictors[predictor_address];
    bool correctness = predict(pc) == outcome;
    predictors[predictor_address] = saturate_predictor(predictor_value, correctness);   
}
