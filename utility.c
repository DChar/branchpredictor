#include <stdint.h>

uint8_t push_shifter(uint8_t pattern, bool outcome){
    uint8_t original_pattern  = pattern;
    uint8_t outcome_byte = (unsigned char)outcome;
    uint8_t new_pattern = (original_pattern << 1) | outcome_byte;
    return new_pattern;
}

const char *byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

uint8_t saturate_predictor(uint8_t predictor_value, bool result){
    uint8_t predictor = predictor_value;
    if(result){
        if(predictor == 0b10){
            predictor++;
        }else if(predictor == 0b01){
            predictor--;
        }
    }else{
        if(predictor == 0b11){
            predictor--;
        }
        else if(predictor == 0b10){
            predictor--;
        }else if(predictor == 0b01){
            predictor++;
        }
        else if(predictor == 0b0){
            predictor++;
        }
        else{
            return -1;
        }
    }
    return predictor;
}
